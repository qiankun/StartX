package com.startx.core.point.anotation;

/**
 * 请求方法枚举
 */
public enum RequestMethod {
	GET, HEAD, POST, PUT, PATCH, DELETE, OPTIONS, TRACE, WEBSOCKET
}
