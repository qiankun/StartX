LC_ALL=en_US.UTF-8

_ServerClass=com.startx.test.J4

# OS specific support.  $var _must_ be set to either true or false.
cygwin=false
os400=false
darwin=false
case "`uname`" in
CYGWIN*) cygwin=true;;
OS400*) os400=true;;
Darwin*) darwin=true;;
esac

# resolve links - $0 may be a softlink
PRG="$0"

while [ -h "$PRG" ]; do
  ls=`ls -ld "$PRG"`
  link=`expr "$ls" : '.*-> \(.*\)$'`
  if expr "$link" : '/.*' > /dev/null; then
    PRG="$link"
  else
    PRG=`dirname "$PRG"`/"$link"
  fi
done

# Get standard environment variables
PRGDIR=`dirname "$PRG"`

# Only set StartX_HOME if not already set
[ -z "$StartX_HOME" ] && StartX_HOME=`cd "$PRGDIR/.." ; pwd`


# Make sure prerequisite environment variables are set
if [ -z "$JAVA_HOME" -a -z "$JRE_HOME" ]; then
  # Bugzilla 37284 (reviewed).
  if $darwin; then
    if [ -d "/System/Library/Frameworks/JavaVM.framework/Versions/CurrentJDK/Home" ]; then
      export JAVA_HOME="/System/Library/Frameworks/JavaVM.framework/Versions/CurrentJDK/Home"
    fi
  else
    JAVA_PATH=`which java 2>/dev/null`
    if [ "x$JAVA_PATH" != "x" ]; then
      JAVA_PATH=`dirname $JAVA_PATH 2>/dev/null`
      JRE_HOME=`dirname $JAVA_PATH 2>/dev/null`
    fi
    if [ "x$JRE_HOME" = "x" ]; then
      # XXX: Should we try other locations?
      if [ -x /usr/bin/java ]; then
        JRE_HOME=/usr
      fi
    fi
  fi
  if [ -z "$JAVA_HOME" -a -z "$JRE_HOME" ]; then
    echo "Neither the JAVA_HOME nor the JRE_HOME environment variable is defined"
    echo "At least one of these environment variable is needed to run this program"
    exit 1
  fi
fi
if [ -z "$JAVA_HOME" -a "$1" = "debug" ]; then
  echo "JAVA_HOME should point to a JDK in order to run in debug mode."
  exit 1
fi
if [ -z "$JRE_HOME" ]; then
  JRE_HOME="$JAVA_HOME"
fi

# Set standard commands for invoking Java.
  _RUNJAVA="$JRE_HOME"/bin/java

if [ -z "$StartX_BASE" ] ; then
  StartX_BASE="$StartX_HOME"
fi

StartX_PID="$StartX_BASE"/pid

if [ -z "$StartX_TMPDIR" ] ; then
  # Define the java.io.tmpdir to use for StartX
  StartX_TMPDIR="$StartX_BASE"/temp
fi

StartX_LOGS="$StartX_HOME"/logs

if [ ! -d "$StartX_LOGS" ]; then
  mkdir "$StartX_LOGS"
fi

# Bugzilla 37848: When no TTY is available, don't output to console
have_tty=0
if [ "`tty`" != "not a tty" ]; then
    have_tty=1
fi

# ----- Execute The Requested Command -----------------------------------------

# Bugzilla 37848: only output this if we have a TTY
if [ $have_tty -eq 1 ]; then
  echo "Using StartX_BASE:   $StartX_BASE"
  echo "Using StartX_HOME:   $StartX_HOME"
  echo "Using StartX_TMPDIR: $StartX_TMPDIR"
  echo "Using StartX_LOGS:   $StartX_LOGS"
  echo "Using JAVA_HOME:   $JAVA_HOME"
fi

# Add dependency library to CLASSPATH
if [ -r "$StartX_HOME"/lib ]; then
	for x in "$StartX_HOME"/lib/*
	do
	    CLASSPATH="$CLASSPATH":$x 
	done
else
	echo "debug mode. loading lib from project"
	if [ -r "$StartX_HOME"/../lib ]; then
		cd "$StartX_HOME"/../
		StartX_HOME=`pwd`
		CLASSPATH="$StartX_HOME"/bin
		for x in "$StartX_HOME"/lib/*
		do
		    CLASSPATH="$CLASSPATH":$x 
		done
	else
		echo "loading lib failed"
		exit 1
	fi
fi
CLASSPATH="$CLASSPATH":"$StartX_HOME"/lib:"$StartX_HOME"/conf
JAVA_OPTS=" -server -Xms5120m -Xmx5120m -Xmn4096m -XX:MetaspaceSize=128M -verbose:gc -Xloggc:logs/gc.log -XX:+PrintGCTimeStamps -XX:+PrintGCDetails -XX:+PrintTenuringDistribution -XX:MaxTenuringThreshold=10 -XX:SurvivorRatio=8 -XX:PermSize=128m -XX:MaxPermSize=128m -XX:+UseParNewGC  -Xss1024k"
if [ "$1" = "run" ]; then

  shift
  "$_RUNJAVA" $JAVA_OPTS $StartX_OPTS \
    -Djava.endorsed.dirs="$JAVA_ENDORSED_DIRS" -classpath "$CLASSPATH" \
    -DStartX.base="$StartX_BASE" \
    -DStartX.home="$StartX_HOME" \
    -Djava.io.tmpdir="$StartX_TMPDIR" \
    "$_ServerClass" "$@" start

elif [ "$1" = "start" ] ; then
  
  if [ ! -z "$StartX_PID" ]; then
    if [ -f "$StartX_PID" ]; then
      if [ -s "$StartX_PID" ]; then
        echo "Existing PID file found during start."
        if [ -r "$StartX_PID" ]; then
          PID=`cat "$StartX_PID"`
          ps -p $PID >/dev/null 2>&1
          if [ $? -eq 0 ] ; then
            echo "Hcps appears to still be running with PID $PID. Start aborted."
            exit 1
          else
            echo "Removing/clearing stale PID file."
            rm -f "$StartX_PID" >/dev/null 2>&1
            if [ $? != 0 ]; then
              if [ -w "$StartX_PID" ]; then
                cat /dev/null > "$StartX_PID"
              else
                echo "Unable to remove or clear stale PID file. Start aborted."
                exit 1
              fi
            fi
          fi
        else
          echo "Unable to read PID file. Start aborted."
          exit 1
        fi
      else
        rm -f "$StartX_PID" >/dev/null 2>&1
        if [ $? != 0 ]; then
          if [ ! -w "$StartX_PID" ]; then
            echo "Unable to remove or write to empty PID file. Start aborted."
            exit 1
          fi
        fi
      fi
    fi
  fi
  

  shift
  touch "$StartX_BASE"/logs/StartX-appserver.out
  "$_RUNJAVA" $JAVA_OPTS $StartX_OPTS \
    -Djava.endorsed.dirs="$JAVA_ENDORSED_DIRS" -classpath "$CLASSPATH" \
    -DStartX.base="$StartX_BASE" \
    -DStartX.home="$StartX_HOME" \
    -Djava.io.tmpdir="$StartX_TMPDIR" \
    "$_ServerClass" "$@" start \
    >> "$StartX_BASE"/logs/StartX-appserver.out 2>&1 &

    if [ ! -z "$StartX_PID" ]; then
      echo $! > $StartX_PID
    fi
    

elif [ "$1" = "stop" ] ; then

  shift
  FORCE=1
  if [ "$1" = "-force" ]; then
    shift
    FORCE=1
  fi

#  "$_RUNJAVA" $JAVA_OPTS \
#    -Djava.endorsed.dirs="$JAVA_ENDORSED_DIRS" -classpath "$CLASSPATH" \
#    -DStartX.base="$StartX_BASE" \
#    -DStartX.home="$StartX_HOME" \
#    -Djava.io.tmpdir="$StartX_TMPDIR" \
#    "$_ServerClass" "$@" stop

  if [ $FORCE -eq 1 ]; then
    #if [ ! -z "$StartX_PID" ]; then
    if [ -e $StartX_PID -a ! -z $StartX_PID ];then  
       echo "Killing: `cat $StartX_PID`"
       kill -9 `cat $StartX_PID`
       RETVAL=$?  
       echo  
       [ $RETVAL = 0 ] && rm -f $StartX_PID
    else
       echo "Kill failed: \$StartX_PID not set or don't exist!"
    fi
  fi

elif [ "$1" = "hardware" ] ; then

    echo "===========Hardware Status==========="
    echo "CPU core number:        `grep processor /proc/cpuinfo | wc -l` "
    echo `grep MemTotal /proc/meminfo`
    echo "TCP Connections(${PORT_BUSINESS}):        `netstat -an |grep tcp |grep $PORT_BUSINESS |wc -l`"

else

  echo "Usage: StartX-appserver.sh ( commands ... )"
  echo "commands:"
  echo "  run               Start StartX appserver in the current window"
  echo "  start             Start StartX appserver in a separate window"
  echo "  stop              Stop StartX appserver"
  echo "  stop -force       Stop StartX appserver (followed by kill -KILL)"
  echo "  hardware          Show hardware info"
  exit 1

fi